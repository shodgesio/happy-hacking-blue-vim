# Happy Hacking Blue

Happy hacking blue is my take on the excellent [Happy Hacking](https://gitlab.com/yorickpeterse/happy_hacking.vim) theme from [Yorick Peterse](https://gitlab.com/yorickpeterse).

I loved so much about the theme, but personally tend more towards cooler colors
such as blues and muted greens rather than the warm reds and yellows of the original theme.

The only differences to be found here lie in changes to three colors:
- the red color has been converted to a deep blue
- the green color has been converted to a lighter blue
- the yellow color has been converted to a rose red

## Screenshots

|  Go  | Python | JavaScript
|------|--------|------------
| ![Go](screenshots/go.png) | ![Python](screenshots/python.png) | ![JavaScript](screenshots/javascript.png)

## Installation

### Using [vim-plug](https://github.com/junegunn/vim-plug):

    Plug 'git@gitlab.com:shodgesio/happy-hacking-blue-vim.git'

## License

Copyright for portions of this project are held by [Yorick Peterse, 2013](https://gitlab.com/yorickpeterse) as a part of [Happy Hacking](https://gitlab.com/yorickpeterse/happy_hacking.vim).

Happy Hacking Blue and all extra source code in this repository is licensed under
the MIT license unless specified otherwise. A copy of this license can be found
in the file "LICENSE" in the root directory of this repository.

